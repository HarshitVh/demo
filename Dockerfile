FROM adoptopenjdk/openjdk11
COPY . /
ADD /build/libs/*.jar myapp.jar
CMD [ "sh", "-c", "java -Dserver.port=$PORT -jar myapp.jar" ]